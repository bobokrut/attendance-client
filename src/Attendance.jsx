import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";

const Attendance = props => {
  const [attendance, setAttendance] = useState([]);
  useEffect(() => {
    fetch("/attendances", {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: "Basic " + btoa("user:password123")
      }
    })
      .then(function(response) {
        if (response.status != 200) {
          console.log("Noresponse from server");
          return;
        }
        response.json().then(function(data) {
          setAttendance(data);
        });
      })
      .catch(function(err) {
        console.log("Fetch error " + err);
      });
  }, []);

  const columns = [
    {
      Header: "Id",
      accessor: "id" // String-based value accessors!
    },
    {
      Header: "Key",
      accessor: "key"
    },
    {
      Header: "Date",
      accessor: "date"
    }
  ];
  return (
    <div>
      <h4>{props.title}</h4>
      <ReactTable data={attendance} columns={columns} />
    </div>
  );
};

export default Attendance;
