import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Attendance from "./Attendance";

function App() {
  return (
    <div className="App">
      <Attendance title="e1900288" />
    </div>
  );
}

export default App;
